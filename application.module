<?php

function application_base_path()
{
    return variable_get('application_path', 'apply');
}

/**
 * Get all hook_settings_disable()
 */
function _application_disable() {
    $links = array();

    foreach (module_list() as $module) {
        if ($data = module_invoke($module, 'settings_disable', '')) {
            $links = array_merge($data, $links);
        }
    }

    return $links;
}

/**
 * Implements hook_user()
 */
function application_user($op)
{
    if ($op == 'categories')
        return application_user_categories();
}

function application_user_categories()
{
    $items = array();
          
/*
    $items[] = array(
        'name' => 'password', 
        'title' => t('Password'), 
        'weight' => 1,
        'page arguments' => array('application_password_form', 1, 2),
        'file path' => drupal_get_path("module", "application") . '/includes',
        'file' => 'password.settings.inc'
    );
*/
    
    if(0 && variable_get('user_pictures', 1))
    {
        $items[] = array(
            'name' => 'picture', 
            'title' => t('Picture'), 
            'weight' => 1,
            'page callback' => 'drupal_get_form',
            'page arguments' => array('application_picture_form', 1),
            'file path' => drupal_get_path("module", "application") . '/includes',
            'file' => 'picture.settings.inc',
        );
    }

    $items[] = array(
        'name' => 'review',
        'title' => t('Review and Submit'),
        'weight' => 100,
        'page callback' => 'drupal_get_form',
        'page arguments' => array('application_review_form', 1),
        'file path' => drupal_get_path("module", "application").'/includes',
        'file' => 'review.settings.inc',
    );

    if(module_exists("openid")) {
        $items[] = array(
            'name' => 'openid', 
            'title' => t('OpenID Identities'), 
            'weight' => 10,
            'page callback' => 'application_openid_form',
            'page arguments' => array(1),
            'file path' => drupal_get_path("module", "application") . '/includes',
            'file' => 'openid.settings.inc',
        );
    }
    
    return $items;
}


/**
 * Implements hook_menu()
 */
function application_menu()
{
    $path = application_base_path();

    $items['admin/settings/application'] = array
    (
        'title' => 'Application Process settings',
        'description' => 'Change or disable.',
        'access arguments' => array('administer site configuration'),
        'page callback' => 'drupal_get_form',
        'page arguments' => array('application_settings'),
        'file' => 'application.admin.inc',
    );
    if (module_exists('views'))
    {
        $items[variable_get('application_view_path', '')] = array(
            'title' => 'Completed Applications',
        );
    }

    $items[$path] = array(
        'title' => "Apply",
        'page callback' => 'application_redirect',
        'access callback' => 'application_redirect_access',
        'type' => MENU_NORMAL_ITEM,
        'menu_name' => 'user-menu',
        'weight' => -100,
    );

    $items["$path/%user"] = array(
        'title' => 'Application', 
        'page callback' => 'user_edit', 
        'page arguments' => array(1),
        'access callback' => 'user_edit_access', 
        'access arguments' => array(1),
        'load arguments' => array('%map', '%index'),
        'file path' => drupal_get_path("module", "user"),
        'file' => 'user.pages.inc',
    );
    
    $empty_account = new stdClass();
    if ($categories = _user_categories($empty_account))
    {
        foreach ($categories as $key => $category)
        {
            if ($category['name'] != 'account')
            {
                $user_edit_menu_item = array(
                    'title callback' => 'check_plain',
                    'title arguments' => array($category['title']),
                    'page callback' => 'user_edit',
                    'page arguments' => array(1, 2),
                    'access callback' => 'user_edit_access',
                    'access arguments' => array(1),
                    'type' => MENU_LOCAL_TASK,
                    'weight' => $category['weight'],
                    'load arguments' => array('%map', '%index'),
                    'file path' => drupal_get_path("module", "user"),
                    'file' => 'user.pages.inc',
                );
                $items["$path/%user/{$category['name']}"] = array_merge($user_edit_menu_item, $category);
            }
        }
    }
      
    return $items;
}

/**
 * Implements hook_menu_alter()
 */
function application_menu_alter(&$items)
{
    // Since disabling 'user/uid/edit' for all users is not an option because
    // some modules might use this route in very different ways
    // we modify the access to affect only users who don't have administer user access
    
    // Disable access to 'user/uid/edit' for everyone except users with
    // administer users access
    $items['user/%user_category/edit']['access callback'] = 'application_edit_access';
    // Disable the Tab
    $items['user/%user_category/edit']['type'] = MENU_CALLBACK;

    // Disable all links implemented in hook_settings_disable()
    if ($links = _application_disable())
    {
        foreach ($links as $link)
        {
            // 'account' is already handled by the MENU_DEFAULT_LOCAL_TASK.
            $items[$link]['type'] = MENU_CALLBACK;
        }
    }
}

/**
 * Copy of user_edit_access()
 * Modified to allow access only to admin users
 */
function application_edit_access($account) {
    return (user_access('administer users')) && $account->uid > 0;
}

/**
 * Redirect to the own settings page
 * Used when user just type in 'settings' instead of 'settings/uid'
 */
function application_redirect()
{
    global $user;
    $path = application_base_path();
    
    if (!$user->uid) {
        drupal_goto("user/register");
    } else {
        drupal_goto("$path/{$user->uid}");
    }
}

/**
 * Access callback to give user access
 */
function application_redirect_access()
{
    global $user;
    
    return 1;
    if (user_edit_access($user))
    {
        return TRUE;
    }
}

function application_form_user_profile_form_alter(&$form, &$form_state)
{
    drupal_add_css(drupal_get_path('module', 'application').'/application-menus.css');
    if (variable_get('application_beforeunload', TRUE)) {
        drupal_add_js(drupal_get_path('module', 'application').'/application-beforeunload.js');
    }
    // Remove all fieldsets when using Profiles
    foreach ( element_children($form) as $key => $child ) {
        if($form[$child]['#type'] == "fieldset") {
            $form[$child]['#type'] = "markup";
        }
    }
    //old replacer
    $category = $form['_category']['#value'];
    $account = $form['_account']['#value'];

    drupal_set_title(check_plain(t($category)));
    //$form = array();
    $edit = (empty($form_state['values'])) ? (array) $account : $form_state['values'];

    $path = application_base_path();

    $categories = _user_categories($account);
    if ($form['_category']['#value'] == 'account')
    {
        if (isset($form['picture'])) {
            unset($form['picture']);
        }
    }
    else if ($form['_category']['#value'] == $categories[1]['name'])
    {
        include(drupal_get_path("module", "application") . '/includes/picture.settings.inc');
        application_picture_form($form, $form_state, null);
    }
    
    if ($categories = _user_categories($account))
    {
        $using_next = false;
        foreach ($categories as $profile_cat)
        {
            if ($using_next)
            {
              $form['#redirect'] = "$path/{$account->uid}/{$profile_cat['name']}";
              break;
            }
            if (0 === strcmp($profile_cat['name'], $category))
            {
              $using_next = true;
            }
        }
    }
    if (empty($form['#redirect']))
    {
        $form['#redirect'] = "$path/{$account->uid}/review";
    }

    $form['submit']['#value'] = t('Save and Continue');
    unset($form['delete']);
}

function application_form_profile_field_form_alter(&$form, $state)
{
    $edit = db_fetch_array(db_query('SELECT * FROM {profile_fields} WHERE fid = %d', $form['fid']['#value']));
    $form['showhide'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Always hide'),
        '#default_value' => isset($edit['showhide']) ? $edit['showhide'] : 0,
        '#multiple' => true,
        '#options' => array(
            1 => t('Title'),
            2 => t('Input'),
            3 => t('Help'),
        ),
    );
}

function application_submit($account)
{
    $profile_values = application_get_profile_values($account);
    $cats_seen = array();
    $profile_dump ="";
    foreach ($profile_values as $field)
    {
        if (!isset($cats_seen[$field['category']])) {
            $cats_seen[$field['category']] = true;
            $profile_dump .= "=== {$field['category']} ===\n";
        }
        $profile_dump .= "{$field['title']}:\n";
        $profile_dump .= "    ".strip_tags($field['value'])."\n\n";
    }
    $params = array();
    $params['profile'] = $profile_dump;

    drupal_set_message(t('Your application has been received.'));
    if (variable_get('application_cc_send', FALSE))
    {
        $message = drupal_mail(
            'application', 'application_cc',
            $account->mail,
            user_preferred_language($account),
            $params
        );
        if (!$message['result'])
        {
            drupal_set_message(t('There was a server failure while delivering mail!  Please contact us to help correct the problem.'), 'error');
        } else {
            drupal_set_message(t('A copy has been sent to %address.', array('%address' => $account->mail)));
        }
    }

    if (variable_get('application_staff_send', FALSE))
    {
        $message = drupal_mail(
            'application', 'application_staff',
            variable_get('application_staff_email', ''),
            user_preferred_language($account),
            $params
        );
        if (!$message['result'])
        {
            drupal_set_message(t('There was a server failure while delivering mail!  Please contact us to help correct the problem.'), 'error');
        }
    }

    $promoted_role = variable_get('application_completed_role', '');
    if (!empty($promoted_role))
    {
      $role_id = db_result(db_query("SELECT rid FROM {role} WHERE name = '%s'", $promoted_role));
      $roles = $account->roles + array($role_id => $promoted_role);
      user_save($account, array('roles' => $roles));
    }
}

function application_get_profile_values($user)
{
    $profile_items = array();
    $result = db_query('SELECT * FROM {profile_fields} WHERE visibility <> %d ORDER BY category, weight', PROFILE_HIDDEN);
    while ($field = db_fetch_object($result))
    {
        $value = profile_view_field($user, $field);
        if ($field->type == 'checkbox') {
            $value = ($value ? t('Yes') : t('No'));
        }
        else if (!$value) {
            if (variable_get('application_hide_blanks', FALSE)) {
                continue;
            }
            $value = "<i>" . t("nothing entered") . "</i>";
        }

        $profile_items[] = array
        (
            'category' => $field->category,
            'name' => $field->name,
            'title' => check_plain($field->title),
            'value' => $value,
            'weight' => $field->weight,
        );
    }
    return $profile_items;
}

function application_mail($key, &$message, $params)
{
    $staff_email = variable_get('application_staff_email', '');

    $cc_intro = variable_get('application_cc_intro', '');
    $staff_intro = variable_get('application_staff_intro', '');
    $cc_subject = variable_get('application_cc_subject', '');
    $staff_subject = variable_get('application_staff_subject', '');
    if (module_exists('token'))
    {
        $staff_subject = token_replace($staff_subject);
        $cc_subject = token_replace($cc_subject);
        $staff_intro = token_replace($staff_intro);
        $cc_intro = token_replace($cc_intro);
    }
    switch ($key) {
    case 'application_cc':
        $message['subject'] = $cc_subject;
        $message['body'] = drupal_wrap_mail("{$cc_intro}\n\n{$params['profile']}");
        $message['headers']['From'] = $staff_email;
        break;
    case 'application_staff':
        $message['to'] = $staff_email;
        $message['subject'] = $staff_subject;
        $message['body'] = drupal_wrap_mail("{$staff_intro}\n\n{$params['profile']}");
        $message['headers']['From'] = $staff_email;
        break;
    }
}

function application_token_list($type)
{
    $tokens = array();
    if ($type == 'all' || $type == 'global')
    {
        $tokens['global']['application-base-path'] = t('Path to the application start page.');
    }
    return $tokens;
}

function application_token_values($type)
{
    $tokens = array();
    if ($type == 'global')
    {
        $tokens['application-base-path'] = application_base_path();
    }
    return $tokens;
}

function application_views_api()
{
    return array(
        'api' => 2,
        'path' => drupal_get_path('module', 'application'),
    );
}
