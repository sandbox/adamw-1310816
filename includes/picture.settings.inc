<?php

function application_picture_form(&$form)
{
    $account = $form['_account']['#value'];

    //$form['#attributes']['enctype'] = 'multipart/form-data';
          
    $admin = user_access('administer users');
          
    // Picture/avatar:
    if (variable_get('user_pictures', 0) && !$register) {
        $form['picture'] = array(
            '#type' => 'markup',
            '#weight' => 101,
        );
        drupal_add_css(drupal_get_path("module", "application") . '/application.css');
        if ($account->picture) {
            $form['current_picture'] = array(
                '#value' => theme('user_picture', (object) $account),
                '#prefix' => '<div class="settings-picture">',
                '#suffix' => '</div>',
                '#weight' => 100,
            );
            $form['picture']['picture_delete'] = array(
                '#type' => 'checkbox',
                '#title' => t('Delete picture'),
                '#description' => t('Check this box to delete your current picture.'),
            );
        }
        else {
            $form['picture']['picture_delete'] = array('#type' => 'hidden');
        }
        $form['picture']['picture_upload'] = array(
            '#type' => 'file',
            '#title' => t('Upload picture'),
            '#size' => 48,
            '#description' => t('Maximum dimensions are %dimensions and the maximum size is %size kB.', array('%dimensions' => variable_get('user_picture_dimensions', '85x85'), '%size' => variable_get('user_picture_file_size', '30'))) . ' ' . variable_get('user_picture_guidelines', ''),
        );
        $form['submit']['#weight'] = 110;

        $form['#validate'][] = 'user_profile_form_validate';
        $form['#validate'][] = 'user_validate_picture';
    }
          
    $form['#uid'] = $account->uid;
          
    $form['#submit'][] = 'application_picture_form_submit';
}

function application_picture_form_submit(&$form, &$form_state)
{
    $account = $form['_account']['#value'];

    // Delete picture if requested, and if no replacement picture was given.
    if (!empty($form_state['values']['picture_delete'])) {
        if ($account->picture && file_exists($account->picture)) {
            file_delete($account->picture);
        }
        $form_state['values']['picture'] = '';
    }

    user_module_invoke('submit', $form_state['values'], $account, $form['_category']['#value']);
    user_save($account, $form_state['values'], $category);
    
    // If imagecache is installed chanches are is being used with user picturs
    // so flush the image cache to regenerate the image
    if(module_exists("imagecache")) {
        imagecache_image_flush($account->picture);
    }

    // Clear the page cache because pages can contain usernames and/or profile information:
    cache_clear_all();

    drupal_set_message(t('The changes have been saved.'));
    return;
}
