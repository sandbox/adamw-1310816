<?php

function application_review_form(&$form_state, $account)
{
    drupal_add_css(drupal_get_path('module', 'application').'/application-menus.css');

    $form = array();
    $form['_account'] = array(
        '#type' => 'value',
        '#value' => $account,
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit application'),
        '#weight' => 200,
    );

    $profile_items = application_get_profile_values($account);

    foreach ($profile_items as $field)
    {
        if (!isset($form[$field['category']])) {
            $form[$field['category']] = array(
                '#type' => 'fieldset',
                '#title' => check_plain(t($field['category'])),
            );
        }

        $form[$field['category']][$field['name']] = array
        (
            '#type' => 'user_profile_item',
            '#title' => check_plain($field['title']),
            '#value' => $field['value'],
            '#weight' => $field['weight'],
            '#attributes' => array('class' => 'profile-'. $field['name']),
        );
    }
    $form['#submit'][] = 'application_review_form_submit';
    $form['#redirect'] = variable_get('application_thankyou_path', '');

    return $form;
}

function application_review_form_submit($form, &$form_state)
{
    $account = $form_state['values']['_account'];
    application_submit($account);
}
