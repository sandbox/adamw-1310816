<?php

/**
 * Callback to show any fields attached to the user
 */
function application_openid_form($account) {	
    include_once(drupal_get_path("module", "openid") . "/openid.pages.inc");
    return openid_user_identities($account);
}

