$(document).ready(function()
{
  $('form#user-profile-form input, form#user-profile-form textarea, form#user-profile-form select').change(function() {
    window.onbeforeunload = function()
    {
      return "Do you want to lose this data?";
    }
  });
  $('form#user-profile-form').submit(function() {
    window.onbeforeunload = null;
  });
});
