<?php

function application_views_default_views()
{
    $views = array();

    $view = new view;
    $view->name = 'application_completed';
    $view->description = 'Completed Applications';
    $view->tag = '';
    $view->base_table = 'users';
    $view->core = 6;
    $view->api_version = '2';
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
    $handler = $view->new_display('default', 'Defaults', 'default');
    $handler->override_option('fields', array(
      'uid' => array(
        'label' => 'Uid',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'absolute' => 0,
          'link_class' => '',
          'alt' => '',
          'rel' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'html' => 0,
          'strip_tags' => 0,
        ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'hide_alter_empty' => 1,
        'link_to_user' => 1,
        'exclude' => 1,
        'id' => 'uid',
        'table' => 'users',
        'field' => 'uid',
        'relationship' => 'none',
      ),
      'name' => array(
        'label' => 'Name',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 1,
          'path' => application_base_path() . "/[uid]/review",
          'absolute' => 0,
          'link_class' => '',
          'alt' => '',
          'rel' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '_blank',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'html' => 0,
          'strip_tags' => 0,
        ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'hide_alter_empty' => 1,
        'link_to_user' => 0,
        'overwrite_anonymous' => 0,
        'anonymous_text' => '',
        'exclude' => 0,
        'id' => 'name',
        'table' => 'users',
        'field' => 'name',
        'relationship' => 'none',
      ),
      'login' => array(
        'label' => 'Last login',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'absolute' => 0,
          'link_class' => '',
          'alt' => '',
          'rel' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'html' => 0,
          'strip_tags' => 0,
        ),
        'empty' => '',
        'hide_empty' => 0,
        'empty_zero' => 0,
        'hide_alter_empty' => 1,
        'date_format' => 'small',
        'custom_date_format' => '',
        'exclude' => 0,
        'id' => 'login',
        'table' => 'users',
        'field' => 'login',
        'relationship' => 'none',
      ),
    ));
    $completed_role = variable_get('application_completed_role', '');
    if ($completed_role)
    {
        $rid = array_search($completed_role, user_roles());
        $handler->override_option('filters', array(
          'rid' => array(
            'operator' => 'or',
            'value' => array($rid),
            'group' => '0',
            'exposed' => FALSE,
            'expose' => array(
              'operator' => FALSE,
              'label' => '',
            ),
            'id' => 'rid',
            'table' => 'users_roles',
            'field' => 'rid',
            'relationship' => 'none',
            'reduce_duplicates' => 0,
          ),
        ));
    }
    $handler->override_option('access', array(
      'type' => 'perm',
      'perm' => 'access user profiles',
    ));
    $handler->override_option('cache', array(
      'type' => 'none',
    ));
    $handler->override_option('empty', '<em>No results</em>');
    $handler->override_option('empty_format', '1');
    $handler->override_option('use_pager', '1');
    $handler->override_option('style_plugin', 'table');
    $handler = $view->new_display('page', 'Page', 'page_1');
    $handler->override_option('path', variable_get('application_view_path', ''));
    $handler->override_option('menu', array(
      'type' => 'normal',
      'title' => 'Completed Applications',
      'description' => '',
      'weight' => '0',
      'name' => 'primary-links',
    ));
    $handler->override_option('tab_options', array(
      'type' => 'none',
      'title' => '',
      'description' => '',
      'weight' => 0,
      'name' => 'navigation',
    ));
    $views[$view->name] = $view;

    return $views;
}
