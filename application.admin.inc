<?php


# default views

function application_settings()
{
    global $user;
    $intro = <<<EOS
Guides new users through a potentially multipage application process, save as they go, provide a review page, and mail to site admins.
<p>
You can <a href="/admin/user/profile">edit the questions and categories</a>.
<a href="/!path">Preview</a> the application.
EOS;
    $form['intro'] = array
    (
        '#type' => 'item',
        '#value' => t($intro, array("!path" => application_base_path())),
    );
    $form['application_staff_email'] = array
    (
	'#default_value' => variable_get('application_staff_email', $user->mail),
	'#description' => t('Correspondence should be sent to and from which email account?'),
	'#required' => TRUE,
	'#size' => 50,
	'#title' => t('Staff email address'),
	'#type' => 'textfield',
    );
    $form['application_path'] = array
    (
	'#default_value' => variable_get('application_path', 'apply'),
	'#description' => t('URL path under which you want the application pages to appear. This should be configured to match site language used to describe the application process.'),
	'#required' => TRUE,
	'#size' => 20,
	'#title' => t('Application Page'),
	'#type' => 'textfield',
    );
    $form['application_thankyou_path'] = array
    (
	'#default_value' => variable_get('application_thankyou_path', 'thankyou'),
	'#description' => t('URL path to redirect to after an application is submitted. You must create a page at this location.'),
	'#required' => TRUE,
	'#size' => 20,
	'#title' => t('Thank-you Page'),
	'#type' => 'textfield',
    );
    if (module_exists("views"))
    {
        $form['application_view_path'] = array
        (
            '#default_value' => variable_get('application_view_path', 'admin/applicants'),
            '#description' => t('URL path of the applicants report.'),
            '#required' => TRUE,
            '#size' => 20,
            '#title' => t('Reporting Page'),
            '#type' => 'textfield',
        );
    }
    $form['application_beforeunload'] = array
    (
	'#default_value' => variable_get('application_beforeunload', TRUE),
	'#description' => t('Warn the user before navigations which might cause responses to be lost.'),
	'#title' => t('Protect Changes'),
	'#type' => 'checkbox',
    );
    $token_desc = "";
    if (module_exists("token"))
    {
        $form['token_help'] = array(
            '#title' => t('Email substitution patterns'),
            '#description' => t('A list of substitutions that may be used in email subject and body text.'),
            '#type' => 'fieldset',
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
        );
        $form['token_help']['help'] = array(
            '#value' => theme('token_help', array('global', 'application')),
        );
        $token_desc = " " . t("Substitution is performed, see above for the keywords.");
    }
    else
    {
      $form['intro']['#value'] .= t("<p>When the optional Token module is enabled, variable substitution will be performed on messages.");
    }
    $form['application_cc'] = array(
        '#title' => t('Courtesy Email Settings'),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );
    $form['application_cc']['application_cc_send'] = array(
        '#type' => 'checkbox',
        '#default_value' => variable_get('application_cc_send', TRUE),
        '#title' => t('Send Courtesy Email'),
        '#description' => t('Send an email to user with a copy of their reponses.'),
    );
    $form['application_cc']['application_cc_subject'] = array
    (
	'#default_value' => variable_get('application_cc_subject', t('Thank you for your application')),
	'#description' => t('Subject line of thank-you email sent to the applicant.') . $token_desc,
	'#required' => TRUE,
	'#size' => 20,
	'#title' => t('Courtesy Message Subject'),
	'#type' => 'textfield',
    );
    $default_cc_intro = <<<EOS
Dear [user-name],
    Thank you for your interest in our program!  Your application has been received and a copy of your answers are included below for your reference.
EOS;
    $form['application_cc']['application_cc_intro'] = array
    (
	'#default_value' => variable_get('application_cc_intro', t($default_cc_intro)),
	'#description' => t('Thank-you email sent to the applicant as a courtesy.') . $token_desc,
	'#required' => TRUE,
	'#cols' => 60,
	'#rows' => 10,
	'#title' => t('Courtesy Message Body'),
	'#type' => 'textarea',
    );
    $form['application_staff'] = array(
        '#title' => t('Staff Email Settings'),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
    );
    $form['application_staff']['application_staff_send'] = array(
        '#type' => 'checkbox',
        '#default_value' => variable_get('application_staff_send', TRUE),
        '#title' => t('Send Staff Email'),
        '#description' => t('Send an email to site admin when an application is submitted.'),
    );
    $form['application_staff']['application_staff_subject'] = array
    (
	'#default_value' => variable_get('application_staff_subject', t('Completed - [user-name]')),
	'#description' => t('Subject line of thank-you email sent to the applicant.') . $token_desc,
	'#required' => TRUE,
	'#size' => 20,
	'#title' => t('Staff Message Subject'),
	'#type' => 'textfield',
    );
    $default_staff_intro = <<<EOS
Received a completed application from [user-name] ([user-mail]).
View:
    [site-url][application-base-path]/[user-id]/review
EOS;
    $form['application_staff']['application_staff_intro'] = array
    (
	'#default_value' => variable_get('application_staff_intro', t($default_staff_intro)),
	'#description' => t('Message sent to staff along with application.') . $token_desc,
	'#required' => TRUE,
	'#cols' => 60,
	'#rows' => 10,
	'#title' => t('Staff Message Body'),
	'#type' => 'textarea',
    );

    $roles = array('') + user_roles();
    $form['application_completed_role'] = array
    (
	'#default_value' => variable_get('application_completed_role', ''),
	'#description' => t('Optionally give users a site role once they submit their application.'),
	'#size' => 1,
	'#title' => t('Set Completed Role'),
	'#options' => array_combine($roles, $roles),
        '#multiple' => FALSE,
        '#required' => FALSE,
	'#type' => 'select',
    );

    $form['application_hide_blanks'] = array
    (
	'#default_value' => variable_get('application_hide_blanks', FALSE),
	'#description' => t('Ignore questions with incomplete responses, these questions will only be visible during editing.'),
	'#title' => t('Hide Blank Repsonses'),
	'#type' => 'checkbox',
    );
    $form['#submit'][] = 'application_admin_submit';
    return system_settings_form($form);
}

function application_admin_submit($form, &$form_state)
{
    cache_clear_all();
    if (module_exists('views')) {
        views_invalidate_cache();
    }
    menu_rebuild();
    //...&dance
}
